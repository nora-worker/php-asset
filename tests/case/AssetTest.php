<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Asset;

use Nora;

/**
 * Assetのテスト
 *
 */
class AssetTest extends \PHPUnit_Framework_TestCase
{
    public function testAsset ( )
    {
        // ロガーを作成してWEBモジュールにアタッチする
        Nora::Logging_start( )->addLogger([
            'writer' => 'stdout'
        ]);

        // アセットマネージャの取得
        $am = Nora::module('Asset')->AssetManager();

        // Sassコマンドの取得
        $sass = $am->sass();


        // サンプルSASSのコンパイル
        $ret = $sass->inputFile(TEST_PROJECT_PATH.'/sample.sass');

        if ($ret === 0)
        {
            // echo $sass->getOutput();
        }else{
            //echo $sass->getError();
        }


        // Coffeeコマンドの取得
        $coffee = $am->coffee();

        // サンプルcoffeeのコンパイル
        $ret = $coffee->inputFile(TEST_PROJECT_PATH.'/sample.coffee');

        if ($ret === 0)
        {
            // $coffee->getOutput();
        }else{
            // $coffee->getError();
        }
    }
}
