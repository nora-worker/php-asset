<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Asset;

use Nora\Core\Module\Module;

/**
 * アセットモジュール
 */
class Facade extends Module
{
    protected function initModuleImpl( )
    {
        $this->Configure()->write('asset.sass', [
            'path' => '/usr/bin/sass',
            'options' => [
                '--load-path',
                realpath(__dir__.'/..').'/sass'
            ]
        ]);

        $this->Configure()->write('asset.coffee', [
            'path' => '/usr/bin/coffee',
            'options' => [
            ]
        ]);

        $this->setComponent('AssetManager', ['scope', function ($scope) {
            $am = new AssetManager( );
            $am->setScope($scope);
            return $am;
        }]);
    }
}
