<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Asset;

use Nora\Core\Component\Component;

/**
 * アセットモジュール
 */
class AssetManager extends Component
{
    protected function initComponentImpl( )
    {
    }

    public function sass( )
    {
        $sass = $this->shell_command(
            $this->configure_read('asset.sass.path'),
            [
                '--compass',
                '--stdin',
                '-E',
                'utf-8'
            ]
        );
        $sass->options()->set($this->configure()->readArray('asset.sass.options'));

        return $sass;
    }

    public function coffee( )
    {
        $coffee = $this->shell_command(
            $this->configure_read('asset.coffee.path'),
            [
                '--compile',
                '--stdio',
            ]
        );

        $coffee->options()->set($this->configure_readArray('asset.coffee.options'));
        return $coffee;
    }
}
